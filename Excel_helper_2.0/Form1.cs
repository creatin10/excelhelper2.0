﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Excel_helper_2._0
{ 

    public partial class Form1 : Form
    {
        // static SheetData SheetData = null
        ExcelWorksheet worksheet;
        ExcelPackage package;
        bool file_opened = false;

        List<string> cols = new List<string> {"Пациент","Дата рождения", "Полис", "Номер случая", "Дата посещения", "Врач", "Ошибки"};

        public Form1()
        {
            InitializeComponent();
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            if (file_opened)
            {
                MessageBox.Show("Нужно сначала закрыть файл");
                return;
            }
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "Excel files (*.xls);(*xlsx)|*.xls;*.xlsx";
                openFileDialog.RestoreDirectory = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    FileInfo file = new FileInfo(openFileDialog.FileName);


                    if (file.FullName.EndsWith("s"))
                    {
                        ConvertXLS_XLSX(file.FullName);
                        file = new FileInfo(openFileDialog.FileName + "x");
                    }

                    package = new ExcelPackage(file);
                    worksheet = package.Workbook.Worksheets.First();
                    file_opened = true;
                    label_ready();
                }
                else MessageBox.Show("Файл не выбран");
            }
        }

        private void ConvertXLS_XLSX(string file_name)
        {
            var app = new Microsoft.Office.Interop.Excel.Application();
            var wb = app.Workbooks.Open(file_name);
            wb.SaveAs(Filename: file_name + "x", FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
            wb.Close();
            app.Quit();
        }

        private void DeleteColumns_Click(object sender, EventArgs e)
        {
            for (int i = worksheet.Dimension.End.Column; i > 0; i--)
            {
                if (!cols.Contains((worksheet.Cells[1, i].Text)))
                {
                    worksheet.DeleteColumn(i);
                    
                }
            }
            label_ready();
            //package.Save();
        }

        private void DeleteEmptyRows_Click(object sender, EventArgs e)
        {

            for (int i = worksheet.Dimension.Start.Row; i <= worksheet.Dimension.End.Row;)
            {
                if (worksheet.Cells[i, 7].Text == string.Empty)  // 7 = столбец Ошибки
                {
                    worksheet.DeleteRow(i);
                    continue;
                }
                i++;
            }
            label_ready();
            //package.Save();
        }

        private void DeleteRows_Click(object sender, EventArgs e)
        {
            worksheet.Cells[2,1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column].Sort(new int[] {3, 4}, new bool[] {false, false } );
            //сортируем по номеру случая и по дате внутри случая
            for (int i = 2; i < worksheet.Dimension.End.Row;)
            {
                if (worksheet.Cells[i, 4].Text != worksheet.Cells[i + 1, 4].Text) //если случай один, то оставляем его
                {
                    i++;
                    continue;
                }
                if (worksheet.Cells[i, 7].Text == "Некоторые услуги случая содержат ошибки")
                { // если случаев несколько и самая ранняя с таким текстом, то ее удаляем
                    worksheet.DeleteRow(i);
                    continue;
                }
                int count_delRows = Count_rowsForDelete(i, worksheet.Cells[i, 4].Text);
                worksheet.DeleteRow(i + 1, count_delRows, true);
                i++;
            }
            label_ready();
            //package.Save();
        }

        private int Count_rowsForDelete(int position, string case_number)
        {
            int count = 0;
            while (worksheet.Cells[position + 1, 4].Text == case_number && position != worksheet.Dimension.End.Row)
            {
                position++;
                count++;
            }
            return count;
        }

        private void SaveFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel files (*xlsx)|*.xlsx";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.FileName = null;
            saveFileDialog.Title = "Save path of the file to be exported";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                package.SaveAs(new FileInfo(saveFileDialog.FileName));

                package?.Dispose();
                worksheet?.Dispose();
                file_opened = false;
                package = null;
                worksheet = null;
                label_ready();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            package?.Dispose();
            worksheet?.Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = null;
            timer1.Enabled = false;
        }

        private void label_ready()
        {
            label1.Text = "Готово";
            timer1.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void RemoveDoubles_Click(object sender, EventArgs e)
        {
            string pattern = @"/[^/]+";
            string newcellvalue;

            for (int i = 2; i < worksheet.Dimension.End.Row; i++)
            {
                newcellvalue = string.Empty;
                foreach (Match match in Regex.Matches("/ " + worksheet.Cells[i, 7].Text + " ", pattern))
                {
                    if (!newcellvalue.Contains(match.Value))
                        newcellvalue += match.Value;
                }
                worksheet.Cells[i, 7].Value = newcellvalue.Remove(newcellvalue.Length - 1, 1).Remove(0, 2);
            }
            label_ready();
        }

        private void ResizeColumns_Click(object sender, EventArgs e)
        {
            // Размеры столбцов для поликлиники
            // после удаления столбцов, индексы колонок остаются прежними
            worksheet.Column(1).Width = 15.5703125;
            worksheet.Column(2).Width = 10.5703125;
            worksheet.Column(3).Width = 9.140625;
            worksheet.Column(4).Width = 7.42578125;
            worksheet.Column(5).Width = 11.5703125;
            worksheet.Column(6).Width = 16.85546875;
            worksheet.Column(7).Width = 37.140625;

            label_ready();
        }
    }
}
